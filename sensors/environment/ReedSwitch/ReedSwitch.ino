/*
Tested with:
* 

 The circuit:
 * VCC voltage is 3.3V to 5V converter (which can be directly connected to 5V single-chip microcontroller and 3.3V)
 * GND external GND
 * OUT of small plate digital output interface (0 and 1)
 
 
 */

const int OUT_PIN = 7;

int val;

void setup() {
  // initialize serial communication:
  Serial.begin(9600);

  pinMode(OUT_PIN,INPUT);
}

void loop()
{

  val = digitalRead(OUT_PIN);

  Serial.print("Sensor result : ");
  Serial.print(val);
  if (val == HIGH){
    Serial.print(" no magnet");
  } 
  else {
    Serial.print(" magnet");
  }

  Serial.println();

  delay(1000);
}

