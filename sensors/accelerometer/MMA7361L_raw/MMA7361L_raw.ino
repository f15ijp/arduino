
/*
X- Analog(3)
 Y-Analog(2)
 Z- Analog(1)
 Vss- 3v3 Arduino
 Gnd- GND
 */

int XPin = 3; // X 
int YPin = 2; // Y 
int ZPin = 1; // Z 
int valX = 0; 
int valY = 0; 
int valZ = 0; 

void setup() { 
  Serial.begin(9600); 
} 

void loop() { 
  valX = analogRead(XPin); 
  valY = analogRead(YPin); 
  valZ = analogRead(ZPin); 
  Serial.print("X-"); 
  Serial.print(valX); 
  Serial.print(" Y-"); 
  Serial.print(valY); 
  Serial.print(" Z-"); 
  Serial.print(valZ); 
  Serial.print("\n"); 
  delay(100); 
} 

